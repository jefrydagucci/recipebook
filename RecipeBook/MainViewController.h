//
//  MainViewController.h
//  RecipeBook
//
//  Created by Walden Global Service on 1/13/14.
//  Copyright (c) 2014 Jefrydaguccystem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController
<UITextFieldDelegate,
UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableArray *tableData;
@end
