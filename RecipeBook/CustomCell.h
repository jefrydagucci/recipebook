//
//  CustomCell.h
//  CoreDataTraining
//
//  Created by Walden Global Service on 12/11/13.
//  Copyright (c) 2013 Walden Global Service. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *labelTitle;
@property (strong, nonatomic) IBOutlet UILabel *labelIngredient;
@property (strong, nonatomic) IBOutlet UIImageView *imgRecipe;
@end
