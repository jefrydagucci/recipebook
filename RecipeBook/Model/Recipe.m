//
//  Recipe.m
//  RecipeBook
//
//  Created by Walden Global Service on 1/13/14.
//  Copyright (c) 2014 Jefrydaguccystem. All rights reserved.
//

#import "Recipe.h"

#import "RecipeBookAppDelegate.h"

@implementation Recipe

- (id)init{
    
    if(self){
        _recipeIngredients = @"";
        _recipeThumbnail = @"";
        _recipeTitle = @"";
        _recipeURL = @"";
    }
    return self;
}

- (NSMutableArray *)getRecipesWithIngredient:(NSString *)ingredient{
    NSData *data = [RecipeBookAppDelegate getResponseFromURL:[host stringByAppendingPathComponent:[NSString stringWithFormat:@"?i=%@", ingredient]] withBody:nil withMethod:@"GET"];
    NSMutableArray *recipes = [self parseRecipesData:data];
    return recipes;
}

- (NSMutableArray *)parseRecipesData:(NSData *)data{
    
    NSMutableArray *recipes = [[NSMutableArray alloc]init];
    
    NSError *err = nil;
    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&err];
    
    NSMutableArray *recipeData = jsonData[@"results"];
    
    for(NSMutableDictionary *recipe in recipeData){
        
        Recipe *objRecipe = [[Recipe alloc]init];
        
        for(NSString *key in recipe){
            
            NSString *value = recipe[key];
            
            if([key isEqualToString:@"href"]){
                [objRecipe setRecipeURL:value];
            }
            else if ([key isEqualToString:@"ingredients"]){
                [objRecipe setRecipeIngredients:value];
            }
            else if([key isEqualToString:@"thumbnail"]){
                [objRecipe setRecipeThumbnail:value];
            }
            else if([key isEqualToString:@"title"]){
                [objRecipe setRecipeTitle:value];
            }
        }
        
        [recipes addObject:objRecipe];
    }
    return recipes;
}
@end
