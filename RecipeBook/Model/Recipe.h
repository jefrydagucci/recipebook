//
//  Recipe.h
//  RecipeBook
//
//  Created by Walden Global Service on 1/13/14.
//  Copyright (c) 2014 Jefrydaguccystem. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Recipe : NSObject

@property (nonatomic, strong) NSString *recipeURL, *recipeIngredients, *recipeThumbnail, *recipeTitle;

- (NSMutableArray *)getRecipesWithIngredient:(NSString *)ingredient;
@end
