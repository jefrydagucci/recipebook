//
//  DetailRecipeViewController.h
//  RecipeBook
//
//  Created by Walden Global Service on 1/13/14.
//  Copyright (c) 2014 Jefrydaguccystem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailRecipeViewController : UIViewController
<UIWebViewDelegate>

@property (strong, nonatomic) NSString *url;

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;

- (IBAction)btnBackClick:(id)sender;

@end
