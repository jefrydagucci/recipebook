//
//  MainViewController.m
//  RecipeBook
//
//  Created by Walden Global Service on 1/13/14.
//  Copyright (c) 2014 Jefrydaguccystem. All rights reserved.
//

#import "MainViewController.h"
#import "Recipe.h"
#import "DetailRecipeViewController.h"

#import "CustomCell.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableData = [[NSMutableArray alloc]init];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - text field delegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSString *ingredient = textField.text;
    
    if(![ingredient isEqualToString:@""]){
        Recipe *newObjRecipe = [[Recipe alloc] init];
        NSMutableArray *arrOfRecipes = [newObjRecipe getRecipesWithIngredient:ingredient];
        self.tableData = arrOfRecipes;
        [self.tableView reloadData];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField endEditing:YES];
}

#pragma mark - table creation
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"cell";
    
    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil){
        cell = [[CustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    Recipe *recipe = self.tableData[indexPath.row];
    cell.labelTitle.text        = recipe.recipeTitle;
    cell.labelIngredient.text   = recipe.recipeIngredients;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if(cell.imgRecipe.image == nil){
            [cell.imgRecipe setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:recipe.recipeThumbnail]]]];
        }
    });
    
    return cell;
}

#pragma mark - table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath *currIndex  = [self.tableView indexPathForSelectedRow];
    Recipe *currentRecipe   = self.tableData[currIndex.row];
    
    if([[segue destinationViewController] isKindOfClass:[DetailRecipeViewController class]]){
        DetailRecipeViewController *detailVC = (DetailRecipeViewController *)[segue destinationViewController];
        detailVC.url = currentRecipe.recipeURL;
    }
}

@end
