//
//  RecipeBookAppDelegate.h
//  RecipeBook
//
//  Created by Walden Global Service on 1/13/14.
//  Copyright (c) 2014 Jefrydaguccystem. All rights reserved.
//

#define host @"http://www.recipepuppy.com/api/"
#import <UIKit/UIKit.h>

@interface RecipeBookAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

+ (NSData *)getResponseFromURL:(NSString *)urlString withBody:(NSData *)requestBody withMethod:(NSString *)method;

@end
